FROM python:slim

ENV KSERVER=192.168.86.42
ENV KPORT=9092

RUN mkdir -p /kafka
WORKDIR /kafka
COPY requirements.txt producer.py import.csv .
RUN pip install --no-cache-dir -r requirements.txt

CMD ["python", "/kafka/producer.py"]
