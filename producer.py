from kafka import KafkaProducer
import kafka.errors, os, csv, json

def import_kafka():
    try:
        producer = KafkaProducer(
            bootstrap_servers=f"{os.environ['KSERVER']}:{os.environ['KPORT']}", 
            key_serializer=lambda v: json.dumps(v).encode('ascii'),
            value_serializer=lambda v: json.dumps(v).encode('ascii')
        )

        csvfile = open('import.csv', newline='')

    except kafka.errors.NoBrokersAvailable:
        print("Verbindung fehlgeschlagen")

    except FileNotFoundError:
        print("Importdatei konnte nicht geöffnet werden!")
        
    else:
        topic = 'mobile_passwords'

        csvreader = csv.DictReader(csvfile, delimiter=';', quotechar="'")        
        for row in csvreader:
            print(f"{row['device']}, {row['name']}, {row['password']}")
            producer.send(
                topic,
                key={"id": row['device']},
                value=
                {
                        "name": row['name'],
                        "password": row['password']
                    }
                )

        producer.flush()
        
        print("Importvorgang wurde erfolgreich durchgeführt.")

    finally:
        print("Importvorgang wurde beendet.")

if __name__ == '__main__':
   import_kafka()
