# producer

## Info

Dekodiert Passwörter(rot13) aus den Messages eines Brokers und speichert die Informationen in einer Datenbank.

## Start des Containers

```
podman run -d -e KSERVER=192.168.86.42 -e KPORT=9092 --name producer registry.gitlab.com/claus.boehmer/producer
```